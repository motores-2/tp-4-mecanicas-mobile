using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void GAME7()
    {
        SceneManager.LoadScene("Mecanica 7");
    }
    public void GAME13()
    {
        SceneManager.LoadScene("Game13");
    }
    public void GAME31()
    {
        SceneManager.LoadScene("GAME31");
    }
    public void GAME34()
    {
        SceneManager.LoadScene("Mecanica 34");
    }
    public void GAME101()
    {
        SceneManager.LoadScene("JuegoNuevo");
    }
}

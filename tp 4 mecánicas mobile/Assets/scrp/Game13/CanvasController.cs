using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CanvasController : MonoBehaviour
{
    public TextMeshProUGUI textoReinicio;
    public TextMeshProUGUI textoEstado;
    public TextMeshProUGUI textoDisparosRestantes;

    private void Update()
    {
        if (ShootController.GameEnd)
        {
            textoReinicio.text = "presiona R para reiniciar";
            textoEstado.text = GameManager.GameState;
            textoDisparosRestantes.text = "";
        }
        if(!ShootController.GameEnd)
        {
            textoReinicio.text = "";
            textoEstado.text = "";
            textoDisparosRestantes.text = "Disparos Restantes: " + ShootController.TirosRestantes.ToString();
        }
    }
        
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetiveController : MonoBehaviour
{
    public float rotacionObjetivo = 5f;

    void Update()
    {
        transform.Rotate(0, 0, rotacionObjetivo * Time.deltaTime);
    }
}

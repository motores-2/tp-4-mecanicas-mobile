using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BulletController : MonoBehaviour
{
    private Rigidbody rb;
    public static bool end = false;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("objetivo"))
        {
            transform.SetParent(collision.gameObject.transform);
        }
        if (collision.gameObject.CompareTag("disparo"))
        {
            ShootController.GameEnd = true;
            GameManager.GameState = "Perdiste :(";
        }
    }
}

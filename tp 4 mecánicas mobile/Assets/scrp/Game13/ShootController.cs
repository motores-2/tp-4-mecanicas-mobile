using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : MonoBehaviour
{
    public float VelocidadBala = 2f;
    public float tiempoEntreDisparos;
    private float tiempoInicial;
    public GameObject prefabDisparo;
    public static int TirosRestantes ;
    public int tirosDelNivel;
    public static bool GameEnd = false;
    public bool gameend = false;

    public List<GameObject> listaDisparos = new List<GameObject>();

    private void Start()
    {
        tiempoInicial = tiempoEntreDisparos;
        TirosRestantes = tirosDelNivel;
    }

    void Update()
    {
        if (!GameEnd)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && tiempoEntreDisparos <= 0f)
            {
                GameObject bala = Instantiate(prefabDisparo, transform.position, transform.rotation);
                bala.GetComponent<Rigidbody>().AddForce(VelocidadBala, 0, 0, ForceMode.Impulse);
                listaDisparos.Add(bala);

                tiempoEntreDisparos = tiempoInicial;
                TirosRestantes--;
            }
            tiempoEntreDisparos -= Time.deltaTime;

            if (TirosRestantes <= 0)
            {
                GameManager.GameState = "Ganaste :)";
                GameEnd = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetGame();
        }

        gameend = GameEnd;
    }

    private void ResetGame()
    {
        GameEnd = false;
        TirosRestantes = tirosDelNivel;
        GameManager.GameState = "";
        foreach (GameObject disparo in listaDisparos)
        {
            Destroy(disparo);
        }
        listaDisparos.Clear();
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelController : MonoBehaviour
{
    public GameObject camaraGanarPerder;
    public TextMeshProUGUI textoGanarPerder;
    public TextMeshProUGUI textoRebotesRestantes;
    public GameObject jugador;
    private bool noRepetir = false;
    private bool noRepetir1 = false;
    private int saltosRestantes;
    private void Start()
    {
        camaraGanarPerder.SetActive(false);
    }
    private void Update()
    {
        if (PlayerMovement.hasWon && !PlayerMovement.hasLost)
        {
            camaraGanarPerder.SetActive(true);
            textoGanarPerder.text = " Felicitaciones ganaste!!                    presiona R para reiniciar el nivel";
        }
        else if(!PlayerMovement.hasWon && PlayerMovement.hasLost)
        {
            camaraGanarPerder.SetActive(true);
            textoGanarPerder.text = "  Perdiste :(                    presiona R para reiniciar el nivel";
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            restartGame();
        }
        if (bounceObject.currentBounces == 0 && !noRepetir)
        {
            saltosRestantes = 3;
            noRepetir = true;
        }
        else if(bounceObject.currentBounces == 0 && noRepetir1)
        {
            saltosRestantes = 0;
        }
        if (bounceObject.currentBounces == 1)
        {
            saltosRestantes = 2;
            noRepetir1 = true;
        }
        if (bounceObject.currentBounces == 2)
        {
            saltosRestantes = 1;
        }
        if (bounceObject.currentBounces == 3)
        {
            saltosRestantes = 0;
        }


        textoRebotesRestantes.text = "Rebotes restantes: " + saltosRestantes.ToString();
    }

    public void restartGame()
    {
        camaraGanarPerder.SetActive(false);
        Instantiate(jugador);
        PlayerMovement.hasLost = false;
        PlayerMovement.hasWon = false;
        bounceObject.currentBounces = 0;
        bounceObject.bounceForce = 500f;
    }
}

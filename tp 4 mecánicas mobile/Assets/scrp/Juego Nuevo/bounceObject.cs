using UnityEngine;

public class bounceObject : MonoBehaviour
{
    public static float bounceForce = 500f;
    public float forceAttenuation = 0.8f;
    public int maxBounces = 3;
    public static int currentBounces = 0;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Rigidbody playerRB = collision.gameObject.GetComponent<Rigidbody>();
            Vector3 direction = collision.contacts[0].point - collision.transform.position;
            direction = -direction.normalized;

            float attenuationFactor = Mathf.Pow(forceAttenuation, currentBounces); 
            float attenuatedForce = bounceForce * attenuationFactor;

            playerRB.AddForce(direction * attenuatedForce);

            currentBounces++;
            if (currentBounces >= maxBounces)
            {
                bounceForce = 0;
                currentBounces = 0;
            }
        }
        if (bounceForce == 0)
        {
            PlayerMovement.canBounce = false;
        }
    }
}



using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 10f;
    private Rigidbody rb;
    private bool isDragging = false;
    private Vector3 dragStartPos;
    private Vector3 dragEndPos;
    private bool canDrag = true;
    public static bool canBounce = true;
    private float timeForEnd;

    public float tiempoEnObjetivo = 0f;
    public static bool hasWon;
    public static bool hasLost;

    private bool isInCollision = false; // Variable para controlar si est� en colisi�n con el objetivo

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        timeForEnd = 5f;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {

            Destroy(this.gameObject);
        }
        if (Input.GetMouseButtonDown(0) && canDrag)
        {
            isDragging = true;
            dragStartPos = GetMouseWorldPosition();
        }

        if (Input.GetMouseButtonUp(0) && canDrag)
        {
            isDragging = false;
            dragEndPos = GetMouseWorldPosition();
            Vector3 direction = dragEndPos - dragStartPos;
            rb.AddForce(direction * moveSpeed);
            canDrag = false;
        }

        if (isDragging)
        {
            Vector3 currentPos = GetMouseWorldPosition();
            Vector3 direction = currentPos - dragStartPos;
            rb.velocity = direction * moveSpeed;
        }
        if (isInCollision)
        {
            tiempoEnObjetivo += Time.deltaTime;
            if (tiempoEnObjetivo >= 5f)
            {
                hasWon = true;
            }
        }
        if (hasWon)
        {
            win();
        }
    }

    Vector3 GetMouseWorldPosition()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = Camera.main.transform.position.y - transform.position.y;
        return Camera.main.ScreenToWorldPoint(mousePosition);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("objetivo"))
        {
            isInCollision = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("objetivo"))
        {
            isInCollision = false;
            tiempoEnObjetivo = 0f; 
        }
    }

    public void win()
    {
        hasWon = true;
        hasLost = false;
        Destroy(this.gameObject);
    }
}

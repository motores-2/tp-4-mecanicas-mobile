using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class plataformController : MonoBehaviour
{
    private Rigidbody rb;
    public float rotation;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        rotation = (Input.GetAxisRaw("Horizontal"))/2;


        if (rotation != 0)
        {
            rb.transform.Rotate(0, 0, rotation);
        }
        if(rotation == 0)
        {
            rb.transform.Rotate(0, 0, 0);
        }

    }
}

using UnityEngine;
using TMPro;

public class CanvasControlleer : MonoBehaviour
{
    public TextMeshProUGUI textoScore;

    void Start()
    {
        textoScore.text = "";
    }

    void Update()
    {
        float score = BallBounce.score;
        float resultado = Mathf.Round(score * 10f) / 10f;
        string resultadoFormateado = resultado.ToString("F1");
        textoScore.text = "Score: " + resultadoFormateado;
    }
}
using UnityEngine;

public class BallBounce : MonoBehaviour
{
    public float bounceForce = 10f;
    public static float score;
    private GameObject camara2;
    public static bool GameEnd = false;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("objetivo"))
        {
            Vector3 bounceDirection = collision.contacts[0].normal;

            GetComponent<Rigidbody>().AddForce(bounceDirection * bounceForce, ForceMode.Impulse);
        }
        if (collision.gameObject.CompareTag("obstaculo"))
        {
            GameEnd = true;
            gameEnd();
            Destroy(this.gameObject);
        }
    }
    private void Update()
    {
        score += Time.deltaTime;
    }
    private void Start()
    {
        camara2 = GameObject.Find("Camera");
        camara2.SetActive(false);
    }
    void gameEnd()
    {
        camara2.SetActive(true);
        Destroy(this.gameObject);
    }

}

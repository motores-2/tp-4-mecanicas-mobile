using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlReinicio : MonoBehaviour
{
    public GameObject jugador;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)&& BallBounce.GameEnd)
        {
            BallBounce.score = 0;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    }

}

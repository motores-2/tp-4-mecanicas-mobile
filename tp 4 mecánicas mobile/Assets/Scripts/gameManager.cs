using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManager : MonoBehaviour
{
    private GameObject camaraPerdiste;
    private bool camaraApagada;
    public GameObject prefabJugador;
    private void Start()
    {
        camaraPerdiste = GameObject.Find("camaraPerdiste");
        camaraPerdiste.SetActive(false);
        camaraApagada = true;

    }
    private void Update()
    {
        if (PlayerMovementController.gameEnd)
        {
            camaraPerdiste.SetActive(true);
            camaraApagada = false;
        }
        else if (!PlayerMovementController.gameEnd && !camaraApagada)
        {
            camaraPerdiste.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.R) && PlayerMovementController.gameEnd)
        {
            Instantiate(prefabJugador);
            camaraPerdiste.SetActive(false);
            camaraApagada = true;
            PlayerMovementController.gameEnd = false;
        }
    }
}

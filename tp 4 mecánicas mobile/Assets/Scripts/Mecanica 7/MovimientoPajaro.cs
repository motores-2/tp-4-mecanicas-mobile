using UnityEngine;

public class MovimientoPajaro : MonoBehaviour
{
    public float life = 20;

    void Update()
    {
        life -= Time.deltaTime;
        if (life < 0) Destroy(gameObject);
        else transform.Translate(0, 0, 1*Time.deltaTime);
    }
}

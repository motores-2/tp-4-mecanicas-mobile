using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class instantiatorManager : MonoBehaviour
{
    public List<GameObject> enemies;
    public GameObject instantiatePos;
    public float respawningTimer;
    public float timeToRespawn;
    private float time = 0;
    private int enemigosGenerados;

    void Start()
    {
        Controller_Enemy.enemyVelocity = 1f;
        timeToRespawn = respawningTimer;
    }

    void Update()
    {
        if (!PlayerMovementController.gameEnd)
        {
            SpawnEnemies();
            ChangeVelocity();
        }
    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = 1f;
    }

    private void MoveInstantiator()
    {
        float randomY = Random.Range(0.5f, 6);
        instantiatePos.transform.position = new Vector3(21, randomY, 0);
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {

            if (enemigosGenerados == 2)
            {
                enemigosGenerados = 0;
                respawningTimer = timeToRespawn;
            }
            else
            {
                GameObject enemigo = Instantiate(enemies[UnityEngine.Random.Range(1, enemies.Count)], transform.position, transform.rotation);
                respawningTimer = timeToRespawn;
                enemigosGenerados++;
            }
        }
    }
}

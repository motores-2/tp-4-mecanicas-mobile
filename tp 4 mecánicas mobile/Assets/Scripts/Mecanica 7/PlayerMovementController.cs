using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    public float jumpForce;
    private Rigidbody rb;
    public static bool gameEnd=false;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("obstaculo"))
        {
            gameEnd=true;
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
    }
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    { 
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.velocity = new Vector3(0,jumpForce,0);
        }
    }
}

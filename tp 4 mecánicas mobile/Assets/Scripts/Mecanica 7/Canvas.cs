using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Canvas : MonoBehaviour
{
    public TextMeshProUGUI textoPerdiste;
    void Start()
    {
        textoPerdiste.text = "";
    }
    void Update()
    {
        if (PlayerMovementController.gameEnd)
        {
            textoPerdiste.text = "¡Perdiste!                                                          pulsa R para reiniciar";
        }
    }
}

﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;
    private float tiempoVida = 6f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force);
        OutOfBounds();

        tiempoVida -= Time.deltaTime;
    }

    public void OutOfBounds()
    {
        if (tiempoVida <= 0)
        {
            DestroyEnemies();
        }
    }
    public void DestroyEnemies()
    {
        Destroy(this.gameObject);
    }

}

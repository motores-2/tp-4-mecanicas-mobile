using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentPajaro : MonoBehaviour
{
    public float sendTimer = 0;
    public float frequency = 9.93f;
    public GameObject floor;

    void Update()
    {
        sendTimer = Time.deltaTime;
        if(sendTimer <= 0)
        {
            Instantiate(floor, new Vector3(3.9f, -0.2f, -12.62f), transform.rotation);
            sendTimer = frequency;
        }
    }
}

using UnityEngine;

public class GravedadCubo : MonoBehaviour
{
    public bool isGravityReversed;
    private Rigidbody rb;

    public Material colorNormal;
    public Material colorInvertido;

    public float fuerzaCambioGravedad = 200f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        isGravityReversed = CompareTag("CuboAzul");

        if (isGravityReversed)
        {
            rb.useGravity=false;
        }
        else
        {
            rb.useGravity=true;
            rb.velocity=Vector3.down;
        }
    }

    private void FixedUpdate()
    {
        // Cambia el color del cubo seg�n si la gravedad est� invertida o no
        if (isGravityReversed)
        {
            rb.AddForce(Vector3.up * fuerzaCambioGravedad, ForceMode.Force);
        }
        else
        {
            rb.AddForce(Vector3.down * fuerzaCambioGravedad, ForceMode.Force);
        }
            
    }

    private void OnMouseDown()
    {
        // Invierte la gravedad y actualiza el color del cubo
        if (isGravityReversed)
        {
            GetComponent<Renderer>().material = colorNormal;
            rb.velocity = Vector3.down;
            isGravityReversed = false;
            tag = "CuboNaranja";
        }
        else
        {
            GetComponent<Renderer>().material = colorInvertido;
            rb.useGravity = true;
            isGravityReversed = true;
            tag = "CuboAzul";
        }
    }
}
